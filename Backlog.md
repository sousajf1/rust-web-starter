* add email to user
* add a create account page with email confirmation
* implement "forgot password" mechanism
* save login attempts table in redis
    * if a user gets the password wrong twice, he gets a warning email
    * three or more times, block that user agent for a while?
* put "/users" page group in another file

