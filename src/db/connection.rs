use rocket_contrib::databases::diesel::PgConnection;

#[database("saa_database")]
pub struct DbConn(PgConnection);

#[database("user_sessions")]
pub struct UserSessionsDbConn(rocket_contrib::databases::redis::Connection);
