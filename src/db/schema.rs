table! {
    users (username) {
        username -> Varchar,
        password -> Varchar,
        is_admin -> Bool,
    }
}
